import csv
import json 


NAME_FILE = "N0117.404 R11 Tabella trattamenti(ENIF) - Ridotta.csv"
START_CHANNELS = 26

class ConsumeRows(object):
    def __init__(self, aSkipLines):
        self.skipLines = aSkipLines
        self.count = 0
        self.Data = {"version": "1.0.0", "applications": []}
        self.theData = self.Data["applications"]
        self.applicationsDict = {}
        self.numRows = None
        self.maxSize = 0
    def consume(self, row):
        if(self.count == 1):
            self.header = [row[i] for i in range(len(row)) if row[i] != ""]
        self.count += 1

        if self.count < self.skipLines:
            return 
        if self.numRows is not None and self.count > self.numRows:
            return

        if(len(row[2])>0):
            self.currentApplication = str(row[4])
            if self.currentApplication not in self.applicationsDict:
                self.theData.append({})
            self.applicationsDict[self.currentApplication] = self.theData[-1]

        if(len(row[4])>0):
            self.applicationsDict[self.currentApplication]["name"] = row[4] 
            self.applicationsDict[self.currentApplication]["id"] = row[3] 
            if("treatments") not in self.applicationsDict[self.currentApplication]:
                self.applicationsDict[self.currentApplication]["treatments"] = []
            self.currentTreatment = self.applicationsDict[self.currentApplication]["treatments"]
            self.currentTreatment.append({})
            self.currentTreatment[-1]["id"] = row[5]
            self.currentTreatment[-1]["name"] = row[6]
            self.currentTreatment[-1]["time"] = row[7]            
            self.currentTreatment[-1]["phases"] = []
        if(len(row[9])>0):
            self.currentTreatment[-1]["phases"].append(dict(zip(self.header[9:START_CHANNELS],
                                                                 row[9:START_CHANNELS])))

            self.currentTreatment[-1]["phases"][-1]["channels"] = []
            channelsList = self.currentTreatment[-1]["phases"][-1]["channels"]
            for i in range(12):
                channelsList.append(dict(zip(self.header[START_CHANNELS+8*i:START_CHANNELS+8*(i+1)],
                                              row[START_CHANNELS+8*i:START_CHANNELS+8*(i+1)] )))
    
parser = ConsumeRows(3)
# parser.numRows = 10

with open(NAME_FILE, 'r') as csvfile:
    myCsv = csv.reader(csvfile, delimiter=';')
    for row in myCsv:
        parser.consume(row)

with open("tables.json", 'w') as myJson:
    myJson.write(json.dumps(parser.Data, sort_keys = True, indent = 4))
            
# print json.dumps(parser.Data, sort_keys = True, indent = 4)
#print parser.Data
